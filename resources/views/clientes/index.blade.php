@extends('layouts.app')

@section('content')
    @if (Session::has('success'))
        <div id="alert" class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container">
        <div class="card">
            <div class="card-header">

                <a class="btn btn-dark" href="{{ route('clientes.create') }}">
                    CREAR CLIENTES
                </a>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Fecha de Creacion</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->nombre}}</td>
                                <td>{{ $cliente->correo}}</td>
                                <td>{{ $cliente->created_at }}</td>
                                <td class="danger">
                                    <a href="{{ route('clientes.edit', $cliente->id) }}" class="btn btn-secondary">EDITAR</a>
                                    <form method="POST" action="{{ route('clientes.destroy', $cliente->id) }}" style="display:inline">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('¿Seguro que deseas eliminar este usuario?')"
                                            class="btn btn-danger btn-xs">ELIMINAR</button>
                                    </form>
        
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No hay registros de usuarios</td>
                            </tr>
                        @endforelse
        
        
                    </tbody>
        
                </table>

            </div>
        </div>
    </div>
@endsection
