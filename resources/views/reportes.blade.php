@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">

            <a class="btn btn-success" href="{{route('descargar_excel')}}">
                EXCEL
            </a>
    

        </div>

        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-striped table-bordered small table-sm">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th>laboratorio</th>
                        <th>Invima</th>
                        <th>Fecha1</th>
                        <th>Fecha2</th>
                        <th>Vigencia</th>
                        <th>COL9</th>
                        <th>COL10</th>
                        <th>COL11</th>
                        <th>COL12</th>
                        <th>COL13</th>
                        <th>COL14</th>
                        <th>COL15</th>
                        <th>COL16</th>
                        <th>COL17</th>
                        <th>COL18</th>
                        <th>COL19</th>
                        <th>COL20</th>
                        <th>COL21</th>
                        <th>COL22</th>
                        <th>COL23</th>
                        <th>COL24</th>
                        <th>COL25</th>
                        <th>COL26</th>
                        <th>COL27</th>
                        <th>COL28</th>
                        <th>COL29</th>
                        <th>COL30</th>
                        <th>COL31</th>
                        <th>COL32</th>
                        <th>COL32</th>
                        <th>COL34</th>
                        <th>COL35</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($datos as $dato)
                        <tr>
                            <td>{{ $dato->id }}</td>
                            <td>{{ $dato->codigo }}</td>
                            <td>{{ $dato->descripcion }}</td>
                            <td>{{ $dato->laboratorio }}</td>
                            <td>{{ $dato->invima }}</td>
                            <td>{{ $dato->fecha1 }}</td>
                            <td>{{ $dato->fecha2 }}</td>
                            <td>{{ $dato->vigencia }}</td>
                            <td>{{ $dato->COL9 }}</td>
                            <td>{{ $dato->COL10}}</td>
                            <td>{{ $dato->COL11 }}</td>
                            <td>{{ $dato->COL12}}</td>
                            <td>{{ $dato->COL13 }}</td>
                            <td>{{ $dato->COL14}}</td>
                            <td>{{ $dato->COL15}}</td>
                            <td>{{ $dato->COL16}}</td>
                            <td>{{ $dato->COL17}}</td>
                            <td>{{ $dato->COL18}}</td>
                            <td>{{ $dato->COL19 }}</td>
                            <td>{{ $dato->COL20}}</td>
                            <td>{{ $dato->COL21 }}</td>
                            <td>{{ $dato->COL22}}</td>
                            <td>{{ $dato->COL23 }}</td>
                            <td>{{ $dato->COL24}}</td>
                            <td>{{ $dato->COL25}}</td>
                            <td>{{ $dato->COL26}}</td>
                            <td>{{ $dato->COL27}}</td>
                            <td>{{ $dato->COL28}}</td>
                            <td>{{ $dato->COL29}}</td>
                            <td>{{ $dato->COL30}}</td>
                            <td>{{ $dato->COL31}}</td>
                            <td>{{ $dato->COL32}}</td>
                            <td>{{ $dato->COL33 }}</td>
                            <td>{{ $dato->COL34}}</td>
                            <td>{{ $dato->COL35}}</td>
                        
                        </tr>

                    @empty
                        <tr>
                            <td colspan="4">No hay registros de usuarios</td>
                        </tr>
                    @endforelse
    
    
                </tbody>
    
            </table>
            </div>
            {{$datos->links()}}

        </div>
    </div>
</div>


@endsection