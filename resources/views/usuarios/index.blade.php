@extends('layouts.app')

@section('content')
    @if (Session::has('success'))
        <div id="alert" class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container">
        <div class="card">
            <div class="card-header">

                <a class="btn btn-dark" href="{{ route('users.create') }}">
                    CREAR USUARIOS
                </a>
            </div>

            <div class="card-body">
                <div class="col my-3">
                <form method="GET" action="{{ route('users.index') }}" >
                  <div class="row">
                                
                    <div class="form-group col">
                       <input type="text" class="form-control" name="buscar" placeholder="buscar usuario..." value="{{ old('buscar') }}">
                    </div>
                    <div class="form-group col">
                        <button type="submit" class="btn btn-primary">BUSCAR
                        </button>
                    </div>
                  </div>
                </form>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Fecha de Creacion</th>
                            <th>Rol</th>
                            <th>Acciones</th>
        
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($usuarios as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->is_admin ? 'si' : 'no' }}</td>
                                <td class="danger">
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-secondary">EDITAR</a>
                                    <form method="POST" action="{{ route('users.destroy', $user->id) }}" style="display:inline">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('¿Seguro que deseas eliminar este usuario?')"
                                            class="btn btn-danger btn-xs">ELIMINAR</button>
                                    </form>
        
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No hay registros de usuarios</td>
                            </tr>
                        @endforelse
        
        
                    </tbody>
        
                </table>

            </div>
        </div>
    </div>
@endsection
