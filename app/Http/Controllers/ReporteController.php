<?php

namespace App\Http\Controllers;

use App\Exports\ReporteExport;
use App\Reporte;
use Illuminate\Http\Request;

class ReporteController extends Controller
{
    public function Index()
    {
        $datos=Reporte::paginate(5);
        return view("reportes", compact("datos"));
    }

    public function descargar_excel()
    {
        return (new ReporteExport)->download('reporte.xlsx');
    }
}
