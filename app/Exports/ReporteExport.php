<?php

namespace App\Exports;

use App\Reporte;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;


class ReporteExport implements FromQuery
{
    use Exportable;

    public function query()
    {
        return Reporte::query();
    }

    

}
